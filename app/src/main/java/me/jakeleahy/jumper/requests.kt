package me.jakeleahy.jumper

import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.serialization.responseObject

fun request(url: String, callback: (results: SearchResults) -> Unit): Result<> {
    @UseExperimental(kotlinx.serialization.ImplicitReflectionSerializer::class)
    val (_, _, request) = url
        .httpGet()
        .responseObject<SearchResults>()
    return request
}

fun search(query: String, host: String, apiKey: String, callback: (results: SearchResults) -> Unit) {
    val time = System.currentTimeMillis()/1000
    request("$host/api/v2.0/indexers/all/results?apikey=$apiKey&Query=$query&_=$time", callback)
}
