package me.jakeleahy.jumper
import kotlinx.serialization.*

// Top level json from search result
@Serializable
data class SearchResults(val Indexers: List<Indexer>,
                         val Results: List<Result>)

// Info about trackers used in search
@Serializable
data class Indexer(val Status: Int,
                   val ID: String,
                   val Name: String,
                   val Results: Int,
                   val Error: String?)

// Item in the search result
@Serializable
data class Result(val FirstSeen: String,
                  val Tracker: String,
                  val TrackerID: String,
                  val CategoryDesc: String,
                  val BlackHoleLink: String?,
                  val Title: String,
                  val Guid: String,
                  val Link: String?,
                  val Comments: String,
                  val PublishDate: String?,
                  val Category: List<Int>,
                  val Size: Int,
                  val Files: String?,
                  val Grabs: String?,
                  val Description: String?,
                  val RageID: String?,
                  val TVDBId: String?,
                  val Imdb: String?,
                  val Seeders: Int,
                  val Peers: Int,
                  val BannerUrl: String?,
                  val InfoHash: String?,
                  val MagnetUri: String?,
                  val MinimumRatio: Float,
                  val MinimumSeedTime: Int,
                  val DownloadVolumeFactor: Float,
                  val UploadVolumeFactor: Float,
                  val Gain: Float)